from alter_char import alternatingCharacters

def test_alternating_character():
    assert alternatingCharacters('AAAA') == 3
    assert alternatingCharacters('BBBBB') == 4
    assert alternatingCharacters('ABABABAB') == 0
    assert alternatingCharacters('BABABA') == 0
    assert alternatingCharacters('AAABBB') == 4
    assert alternatingCharacters('AAAABAABBB') == 6
    assert alternatingCharacters('BABAAABBBB') == 5
    assert alternatingCharacters('ABABBABAAB') ==2
