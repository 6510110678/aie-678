from fizz_buzz import fizzbuzz

def test_fizzbuzz():
    assert fizzbuzz(5) == 'Buzz'
    assert fizzbuzz(0) == 'FizzBuzz'
    assert fizzbuzz(10) == 'Buzz'
    assert fizzbuzz(1) == None
    assert fizzbuzz(15) == 'FizzBuzz'
    assert fizzbuzz(99) == 'Fizz'