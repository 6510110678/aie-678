from funny_str import funnyString

def test_funnyString():
    assert funnyString('acxz') == 'Funny'
    assert funnyString('Hello') == 'Not Funny'
    assert funnyString('bcxz') == 'Not Funny'
    assert funnyString('ivvkxq') == 'Not Funny'
    assert funnyString('ivvkx') == 'Not Funny'
    assert funnyString('jkmsxzwrxzy') == 'Not Funny'
    assert funnyString('pryumtuntmovpwvowslj') == 'Funny'
    assert funnyString('ovyvzvptyvpvpxyztlrztsrztztqvrxtxuxq') == 'Funny'
    assert funnyString('uvzxrumuztyqyvpnji') == 'Funny'
    assert funnyString('ptvzstvotxqyvzrwyqryzrpkswzryupwutmigc') == 'Funny'

