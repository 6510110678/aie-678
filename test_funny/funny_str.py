def funnyString(s):
    r = s[::-1]
    s_ascii = [ord(i) for i in s]
    r_ascii = [ord(i) for i in r]
    s_diff_ascii = [abs(s_ascii[i] - s_ascii[i+1]) for i in range(len(s_ascii) - 1)]
    r_diff_ascii = [abs(r_ascii[i] - r_ascii[i+1]) for i in range(len(r_ascii) - 1)]
    if s_diff_ascii == r_diff_ascii:
        return 'Funny'
    else:
        return 'Not Funny'