def gridChallenge(grid):
    row = []
    row_item= []

    for item in grid:
        items = list(item)
        items.sort()
        ''.join(items)
        row_item.append(items)
        
    for i in range(len(row_item)):
        row_item[i] = ''.join(row_item[i])

    for i in range(len(row_item)-1):
        if not(row_item[i]<=row_item[i+1]):
            return 'NO'

    return 'YES'