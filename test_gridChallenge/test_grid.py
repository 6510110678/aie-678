from gridchallenge import gridChallenge

def test_gridchallenge():
    assert gridChallenge(['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']) == 'YES'
    assert gridChallenge(['asd', 'cvb', 'rty', 'asg']) == 'NO'
    assert gridChallenge(['zzx', 'bgf', 'xxz', 'aas']) == 'NO'
    assert gridChallenge(['a', 'b', 'c', 'd']) == 'YES'
    assert gridChallenge(['zzz', 'zzz', 'zzz', 'zzz']) == 'YES'