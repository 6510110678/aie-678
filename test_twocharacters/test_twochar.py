from two_characters import alternate

def test_two_character():
    assert alternate('beabeefeab') == 5
    assert alternate('a') == 0
    assert alternate('ab') == 2
    assert alternate('aaaaa') == 0
    assert alternate('pvmaigytciycvjdhovwiouxxylkxjjyzrcdrbmokyqvsradegswrezhtdyrsyhg') == 6