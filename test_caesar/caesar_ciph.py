import string

def caesarCipher(k, s):
    s = list(s)
    mapping_lower = list(string.ascii_lowercase)
    mapping_upper = list(string.ascii_uppercase) 
    for i, w in enumerate(s):
        if w.isalpha():
            if w in mapping_lower:
                indexstr = mapping_lower.index(w)
                position = (indexstr + k) % len(mapping_lower)
                s[i] = mapping_lower[position]
            elif w in mapping_upper:
                indexstr = mapping_upper.index(w)
                position = (indexstr + k) % len(mapping_upper)
                s[i] = mapping_upper[position]
        else:
            continue
    newstr = ''.join(s)
    return newstr