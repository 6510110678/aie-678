from caesar_ciph import caesarCipher

def test_Caesar_Cipher():
    assert caesarCipher(4, 'Hello_World!') == 'Lipps_Asvph!'
    assert caesarCipher(26, 'Ciphering.') == 'Ciphering.'
    assert caesarCipher(87, 'www.abc.xy') == 'fff.jkl.gh'
    assert caesarCipher(2, 'middle-Outz') == 'okffng-Qwvb'
    assert caesarCipher(0, 'D3q4') == 'D3q4'
    assert caesarCipher(98, '159357lcfd') == '159357fwzx'
    assert caesarCipher(87, '6DWV95HzxTCHP85dvv3NY2crzt1aO8j6g2zSDvFUiJj6XWDlZvNNr') == '6MFE95QigCLQY85mee3WH2laic1jX8s6p2iBMeODrSs6GFMuIeWWa'