from cat_mouse import catAndMouse

def test_cat_mouse():
    assert catAndMouse(1, 2, 3) == 'Cat B'
    assert catAndMouse(60, 50, 55) == 'Mouse C'
    assert catAndMouse(77, 75, 89) == 'Cat A'